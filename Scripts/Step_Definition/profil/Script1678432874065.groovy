import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login/click_btn_Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Login/input_Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Login/input_Password'), [('password') : password], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Login/click_Masuk'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Profil/click_Icon_Profil'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Profil/click_Informasi_Akun'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Profil/click_Informasi_Profil_Standar'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Profil/input_Nama_Lengkap'), [('nama_lengkap') : nama_lengkap], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Profil/Input_Nama_Panggilan'), [('nama_panggilan') : nama_panggilan], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Profil/input_Notelpon'), [('notelp') : notelp], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Profil/input_Jenis_Kelamin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Profil/select_Wanita'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Profil/input_Tanggal_Lahir'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Profil/click_Icon_Centang'), [:], FailureHandling.STOP_ON_FAILURE)

